<?php

namespace App\Services;

use App\Models\RoutePoint;
use App\Models\Trip;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;


class TripService
{
    public function createTrip(Request $request)
    {
        try {
            DB::beginTransaction();
            $trip = Trip::create([
                'user_id' => auth()->user()->id,
                'date' => $request->input('date'),
                'time' => $request->input('time'),
                'seats_available' => $request->input('seats_available'),
                'price_per_seat' => $request->input('price_per_seat'),
            ]);
            $routePoints = collect($request->input('route_points'));
            $trip->routePoints()->createMany(
                $routePoints->map(function ($routePoint, $key) {
                    return [
                        'order' => $key,
                        'city' => $routePoint['city'],
                    ];
                })->all()
            );
            DB::commit();
            return $trip->load(['routePoints']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function searchedTripsQuery(Request $request)
    {
        // Get the correct trip ids
        $trip_ids = DB::select('
                   SELECT a.trip_id
                   FROM route_points a
                   JOIN route_points b on b.trip_id = a.trip_id
                   WHERE a.city=:from
                   AND b.city=:to
                   AND a.order < b.order',
            ['from' => $request->input('from'),
                'to' => $request->input('to')]);
        // Convert result to array
        $result = array_map(function ($value) {
            return (array)$value;
        }, $trip_ids);
        // Get associated routepoints
        $trips = Trip::with('routePoints')->
        whereIn('id', $result)->
        orderBy('date')->orderBy('time')->
        with('user.person')->
        with('tripPassengers')->
        with('acceptedPassengers.user.person')->
        get()->
        groupBy('date');
        return response()->json($trips, 200);
    }

    public function userTripRequestsQuery(Request $request)
    {
        $userTripRequests = Trip::where('created_by', auth()->user()->id)->whereHas('tripPassengers', function ($query) {
            $query->where('request_status', 'like', 'pending');
        })->with('tripPassengers.user.person')->with('acceptedPassengers.user.person')->with('routePoints')->get();
        if (sizeof($userTripRequests) == 0)
            return null;
        return response()->json($userTripRequests, 200);
    }

    public function userTripsQuery(Request $request)
    {
        $trips = Trip::where('created_by', auth()->user()->id)->with('routePoints')->with('acceptedPassengers.user.person')->with('tripPassengers.user.person')->get();
        if (sizeof($trips) == 0)
            return null;
        return response()->json($trips, 200);
    }

    public function updateCurrentUserTrip(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $trip = Trip::find($id);
            $newCount = sizeof($request->input('route_points'));
            $trip->routePoints()->where('order', '>=', $newCount)->delete();
            foreach ($request->input('route_points') as $key => $routePoint) {
                $trip->routePoints()->updateOrCreate(
                    ['order' => $key],
                    ['city' => $request->input('route_points.' . $key . '.city')]
//                    'location' => $request->input('route_points.'.$key.'.location')],
                );
            }
            $trip->update($request->all());
            $trip->save();
            DB::commit();
            return $trip;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function find($search)
    {
        return (strlen($search) > 2) ? ["data" => Trip::where('name', 'LIKE', '%' . $search . '%')->limit(20)->get()] : [];
    }
}
