<?php

namespace App\Services;

use App\Http\Requests\users\RegisterUser;
use App\Models\User;
use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserService
{
    public function usersQuery(Request $request)
    {
        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        $query = User::with('person');
        return $query->paginate(10);
    }

    public function createUser(Request $request)
    {
        try {
            DB::beginTransaction();
            $user = User::create(array_merge($request->all(), ['password' => 'abc123']));
            $user->person()->create($request->all());
            DB::commit();
            return $user;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function updateUser(Request $request, User $user)
    {
        $user->update($request->all());
        return $user;
    }

    public function updateCurrentUser(Request $request)
    {
        $person = Person::where('user_id', auth()->user()->id)->first();
        $person->update($request->all());
        return $person;
    }

    public function updateCurrentUserPhoto(Request $request)
    {
        $person = Person::where('user_id', auth()->user()->id)->first();
        $pathToPreviousPhoto = $person->photo;
        if ($request->hasFile('photo')) {
            $path = $request->file('photo')->store('photos');
            $person->photo = $path;
            if ($pathToPreviousPhoto) {
                Storage::delete($pathToPreviousPhoto);
            }
            $person->save();
            return (string)$path;
        }
        return null;
    }

    public function deleteCurrentUserPhoto()
    {
        $person = Person::where('user_id', auth()->user()->id)->first();
        $pathToPhoto = $person->photo;
        if ($pathToPhoto) {
            $person->photo = null;
            Storage::delete($pathToPhoto);
        }
        $person->save();
        return $pathToPhoto;
    }

    public function registerUser(RegisterUser $request)
    {
        try {
            DB::beginTransaction();
            $user = User::create($request->all());
            $user->person()->create([
                'email' => $request->input('email'),
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
            ]);
            $user->person;
            $success['token'] = $user->createToken('MyApp')->accessToken;
            DB::commit();
            return response()->json(['success' => $success, 'user' => $user], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function loginUser(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if(!Auth::attempt($request->only(['email', 'password']))){
                return response()->json([
                    'status' => false,
                    'message' => 'Email & Password does not match with our record.',
                ], 401);
            }

            $user = User::where('email', $request->email)->first();

            return response()->json([
                'status' => true,
                'message' => 'User Logged In Successfully',
                'token' => $user->createToken("MyApp")->plainTextToken
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

//    public function find($search)
//    {
//        return (strlen($search) > 2) ? ["data" => User::where('name', 'LIKE', '%' . $search . '%')->limit(20)->get()] : [];
//    }
}
