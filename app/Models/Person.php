<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'people';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    protected $fillable = [
        "user_id",
        "first_name",
        "last_name",
        "email",
        "country",
        "city",
        "date_of_birth",
        "gender",
        "phone",
        "photo",
        "description"
    ];
    protected $casts = [
        "" => "boolean"
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id', 'person_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
