<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    protected $fillable = [
        "user_id",
        "vehicle_id",
        "date",
        "time",
        "seats_available",
        "price_per_seat",
        "status"
    ];
    protected $casts = [
        "" => "boolean"
    ];

    public function creator()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function routePoints()
    {
        return $this->hasMany('App\Models\RoutePoint', 'trip_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function tripPassengers()
    {
        return $this->hasMany('App\Models\TripPassenger', 'trip_id', 'id');
    }

    public function acceptedPassengers()
    {
        return $this->hasMany('App\Models\TripPassenger', 'trip_id', 'id')->where('request_status', 'accepted');
    }
}
