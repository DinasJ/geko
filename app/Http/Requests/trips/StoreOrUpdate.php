<?php

namespace App\Http\Requests\trips;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        $role = auth()->user()->role;
//        return $role === 'admin' || $role === 'user';
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id" => "integer",
            "vehicle_id" => "integer|nullable", // TODO make not nullable
            "route_points" => [
                'required',
                'array',
                'min:2',
                'max:4',
            ],
            "route_points.*.city" => [
                'required',
                'alpha',
                'distinct',
                'string',
                'min:4',
            ],
            "date" => "required|date",
            "time" => "required|date_format:H:i", // TODO add date_format validation
            "seats_available" => "required|integer|min:1|max:7",
            "price_per_seat" => "required|integer|numeric|min:0",
//            "status" => "required|"
        ];
    }
}
