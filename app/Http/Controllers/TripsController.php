<?php

namespace App\Http\Controllers;

use App\Http\Requests\trips\Destroy;
use App\Http\Requests\trips\StoreOrUpdate;
use App\Models\Trip;
use App\Services\TripService;
use Illuminate\Http\Request;

class TripsController extends Controller
{
    private $tripService;

    public function __construct(TripService $tripService)
    {
        $this->tripService = $tripService;
    }

    public function index(Request $request)
    {
        $tripsQuery = $this->tripService->searchedTripsQuery($request);
        return $tripsQuery;
    }

    public function indexCurrentUserTripRequests(Request $request)
    {
        $userTripRequestsQuery = $this->tripService->userTripRequestsQuery($request);
        return $userTripRequestsQuery;
    }

    public function showTrip($id)
    {
        return Trip::findOrFail($id)->load(['routePoints'])->load('user.person')->load('tripPassengers');
    }

    public function currentUserTrips(Request $request)
    {
        $userTripsQuery = $this->tripService->userTripsQuery($request);
        return $userTripsQuery;
    }

    public function showCurrentUserTrip($id)
    {
        return Trip::findOrFail($id)->load(['routePoints']);
    }

    public function updateCurrentUserTrip(StoreOrUpdate $request, $id)
    {
        $userTripsQuery = $this->tripService->updateCurrentUserTrip($request, $id);
        return $userTripsQuery;
    }

    public function deleteCurrentUserTrip(Request $request)
    {
        $trips = Trip::where('created_by', auth()->user()->id)->where('id', $request->input('id'))->delete();
        return response()->json($trips, 200);
    }

    public function show(Trip $trip)
    {
        //$this->authorize('view', $trip);
        return $trip;
    }

    public function store(StoreOrUpdate $request)
    {
        Log::info('Received Request Data:', $request->all());
        $trip = $this->tripService->createTrip($request);
        return $trip;
    }

    public function update(StoreOrUpdate $request, Trip $trip)
    {
        //$this->authorize('update', $trip);

        $trip = $this->tripService->updateTrip($request, $trip);
        return $trip;
    }

    public function destroy(Trip $trip)
    {
        //$this->authorize('delete', $trip);
        $trip->delete();
        return [];
    }

    public function find($search)
    {
        $trips = $this->tripService->find($search);
        return $trips;
    }
}
