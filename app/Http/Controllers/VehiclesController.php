<?php

namespace App\Http\Controllers;

use App\Http\Requests\vehicles\StoreOrUpdate;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class VehiclesController extends Controller
{
    private $vehicleService;

    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }

    public function index(Request $request)
    {
        $vehiclesQuery = $this->vehicleService->vehiclesQuery($request);
        return $vehiclesQuery;
    }

    public function show(Vehicle $vehicle)
    {
        //$this->authorize('view', $vehicle);
        return $vehicle;
    }

    public function store(StoreOrUpdate $request)
    {
        $vehicle = $this->vehicleService->createVehicle($request);
        return $vehicle;
    }

    public function update(StoreOrUpdate $request, Vehicle $vehicle)
    {
        //$this->authorize('update', $vehicle);

        $vehicle = $this->vehicleService->updateVehicle($request, $vehicle);
        return $vehicle;
    }

    public function destroy(Vehicle $vehicle)
    {
        //$this->authorize('delete', $vehicle);
        $vehicle->delete();
        return [];
    }

    public function find($search)
    {
        $vehicles = $this->vehicleService->find($search);
        return $vehicles;
    }

    public function currentUserVehicles(Request $request)
    {
        return Vehicle::where('created_by', auth()->user()->id)->get();
    }
}
