<?php

namespace App\Http\Controllers;

use App\Http\Requests\persons\StoreOrUpdate;
use App\Http\Requests\users\StoreUser;
use App\Http\Requests\users\RegisterUser;
use App\Http\Requests\users\UpdateUser;
use App\Models\Person;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\UserService;

class UsersController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $usersQuery = $this->userService->usersQuery($request);
        return $usersQuery;
    }

    public function show($id)
    {
        //$this->authorize('view', $user);
        return User::findOrFail($id)->load(['person']);
    }

    public function store(StoreUser $request)
    {
        $user = $this->userService->createUser($request);
        return $user;
    }

    public function update(UpdateUser $request, User $user)
    {
        //$this->authorize('update', $user);

        $user = $this->userService->updateUser($request, $user);
        return $user;
    }

    public function destroy(User $user)
    {
        //$this->authorize('delete', $user);
        $user->delete();
        return [];
    }

    public function find($search)
    {
        $users = $this->userService->find($search);
        return $users;
    }

    public function current(Request $request)
    {
        return $request->user()->load('person');
    }

    public function updateCurrent(StoreOrUpdate $request)
    {
        $user = $this->userService->updateCurrentUser($request);
        return $user;
    }

    public function updateCurrentPhoto(Request $request)
    {
        $user = $this->userService->updateCurrentUserPhoto($request);
        return $user;
    }

    public function deleteCurrentPhoto()
    {
        $user = $this->userService->deleteCurrentUserPhoto();
        return $user;
    }

    public function register(RegisterUser $request)
    {
        $user = $this->userService->registerUser($request);
        return $user;
    }

    public function login(Request $request)
    {
        $user = $this->userService->loginUser($request);
        return $user;
    }
}
