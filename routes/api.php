<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UsersController;
use App\Http\Controllers\TripsController;

use App\Models\User;
use App\Models\Trip;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/register', [UsersController::class, 'register']);
Route::post('/login', [UsersController::class, 'login']);
Route::get('/trips', [TripsController::class, 'index']);
Route::middleware('auth:sanctum')->group(function () {
    Route::get('/users/current', [UsersController::class, 'current']);
    Route::put('/users/current', [UsersController::class, 'updateCurrent']);
    Route::post('/users/currentPhoto', [UsersController::class, 'updateCurrentPhoto']);
    Route::post('/users/currentPhoto', [UsersController::class, 'deleteCurrentPhoto']);
    
    Route::prefix('trips')->group(function () {
        Route::post('/my', [TripsController::class, 'store']);
        Route::get('/my', [TripsController::class, 'currentUserTrips']);
        Route::get('/my/{id}', [TripsController::class, 'showCurrentUserTrip']);
        Route::put('/my/update/{id}', [TripsController::class, 'updateCurrentUserTrip']);
        Route::delete('/my/delete', [TripsController::class, 'deleteCurrentUserTrip']);
        Route::get('/requests', [TripsController::class, 'indexCurrentUserTripRequests']);
        Route::get('/{tripId}/requests/update/{passengerId}', [TripPassengersController::class, 'updateTripRequest']);
        Route::get('/requests/delete', [TripPassengersController::class, 'destroyTripRequest']);
        Route::get('/requests/my/cancel', [TripPassengersController::class, 'cancelTripRequest']);
        Route::get('/view/{id}', [TripsController::class, 'showTrip']);
    });
    Route::post('/tripPassengers/book/{id}', [TripPassengersController::class, 'store']);
    Route::get('/vehicles/my', 'VehicleController@currentUserVehicles');
    Route::apiResources([
        'people' => "PeopleController",
        'pickup-points' => 'PickupPointsController',
        'reviews' => 'ReviewsController',
        'route-points' => 'RoutePointsController',
        'trip-passengers' => 'TripPassengersController',
        'user-preferences' => 'UserPreferencesController',
        'vehicles' => 'VehiclesController',
    ]);
    Route::apiResource('trips', 'TripsController')->except(['index']);
    Route::middleware('allowed_roles:admin')->group(function() {
        Route::apiResources([
            'users' => UsersController::class,
        ]);
    });
    Route::get('/users/{id}', 'UsersController@show');
});

Route::post('users/register', [UsersController::class, 'store']);


Route::get('/hello', function () {
    return 'hello';
});