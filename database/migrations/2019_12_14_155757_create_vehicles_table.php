<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('model');
            $table->unsignedSmallInteger('year')->nullable();
            $table->string('color');
            $table->string('license_plate');
            $table->string('type');
            $table->unsignedTinyInteger('back_seats');
            $table->string('luggage');
            $table->boolean('has_winter_tires');
            $table->boolean('allows_bikes');
            $table->boolean('allows_pets');
            $table->string('photo')->nullable();
            $table->boolean('is_primary');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
